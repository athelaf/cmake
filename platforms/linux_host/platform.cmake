# Default platform options
set(PKG_CBLAS_LIB openblas CACHE STRING "Default CBLAS library")

set(LPDNN_PLUGINS "cpu_gemm,cpu_vanilla" CACHE STRING "Configure plugins")

# Tentative gprof support
if(ENABLE_GPROF)
  set(CMAKE_C_FLAGS ${CMAKE_C_FLAGS} -pg)
  set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} -pg)
  set(CMAKE_EXE_LINKER_FLAGS ${CMAKE_EXE_LINKER_FLAGS} -pg)
  set(CMAKE_SHARED_LINKER_FLAGS ${CMAKE_SHARED_LINKER_FLAGS} -pg)
endif()
