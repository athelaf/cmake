Platform Support
================

.. note:: PROVIDED UNDER THE TERMS OF THE BONSEYES NDA. DO NOT REDISTRIBUTE.

Introduction
------------

This directory contains the platforms supported by LPDNN.
A platform corresponds to a specific combination of HW+OS, so a board supporting two different OSs (for example 32bits and 64bits) will appear as two different platforms.

Each subdirectory provides all that is needed to build LPDNN for that specific platform:

- README.md
  Short description of the platform and any specific requirement or instruction.

- platform.cmake
  Build options

- toolchain.cmake
  Cmake toolchain specification

- Dockerfile
  Build environment

- other
  resouces needed in the dockerfile if not possible to download them

platform.cmake
--------------
This file is used at build-time and contains all the settings to customize and **optimize** the build for the platform.
As a minimum it has to specify the HW architecture and the C/C++ toolchain to be used.
The toolchain specification itself is contained in a separate file as required by cmake (see *toolchain.cmake* below).
It's important to carefully choose the build options that allows to maximize the board performances, not just something that works (for example processor architecture, SIMD support etc).

toolchain.cmake
---------------
A toolchain file is used by cmake to pick the right tools (compiler, linker, ect) and build options for the target architecture, and is mandatory for cross-compilation.
Complete info on cmake toolchain files can be found here: https://cmake.org/cmake/help/v3.6/manual/cmake-toolchains.7.html

In practice a toolchain file is quite simple and often just specifies the name of the compiler.
This is also a good place to put build options that have to be valid for *all* subprojects built in the SDK (e.g. ``-mfpu=neon-vfpv4``), including 3rd party packages, since the toolchain file is recursively propagated. Options that should be valid only for the SDK itself (e.g. ``PKG_CBLAS_LIB``) should instead be put in the *platform.cmake* file.

Dockerfile
----------
Supporting cross-compilation for several targets can be messy and time-consuming since each target can require different tools, resources and toolchains that must all be installed and maintained by everybody that wants to work with that platform.
To avoid this issue and ensure that the same set of tools is used by everybody, each platform must also provide a Dockerfile containg all and only the dependencies needed for the build.
Creating a build dockerfile is not more difficult than creating a readme with the list of dependencies, with the advantage that the build environment can be created automatically by docker, saving a lot of installation and maintenance effort.

As a guideline each Dockerfile shall specify the following items:

- The OS used to execute the build.

  This shall be a version of linux, typically a LTS version of ubuntu or debian. Can be anything else as required by the tool/toolchain to be used for the build. Example::

    FROM ubuntu:16.04

- The tools required by the build system.

  They are the same for all the platforms (cmake, python etc), to avoid repeating them many times, simply include the host-specific script that installs them all::

    # Install build dependecies (cmake, tools, python packages, etc)
    WORKDIR /build
    ADD scripts/install_build_deps_ubuntu_xenial.sh install_build_deps.sh
    RUN chmod +x install_build_deps.sh && ./install_build_deps.sh && rm ./install_build_deps.sh
  
  
- The toolchain to be used during the build.

  Sometimes it is already included by default in the selected OS. If the toolchain is downlaoded from an external repository, be careful to always specify a link to a **specific version** of the toolchain, not generic labels such as "*latest*" or "*head*" that could change over time. This guarantees to always use the same version, providing a reproducible build environment.
  Example::
  
    # Install GCC-6 for ARM, version 6.4.1-2018.05
    WORKDIR /opt
    RUN wget https://releases.linaro.org/components/toolchain/binaries/6.4-2018.05/\
             aarch64-linux-gnu/gcc-linaro-6.4.1-2018.05-x86_64_aarch64-linux-gnu.tar.xz
    RUN tar -xvf gcc-linaro-6.4.1-2018.05-x86_64_aarch64-linux-gnu.tar.xz
    ENV PATH=/opt/gcc-linaro-6.4.1-2018.05-x86_64_aarch64-linux-gnu/bin/:$PATH

- The target ``sysroot`` if needed.

  If the target has a linux-like OS, it will have a file system containing, among the rest, the basic libraries needed by the applications running on it, for example standard C/C++ libraries, threading, memory, networking and device libraries.
  This is called the *sysroot*, and it is important that when cross-building we compile/link the application using the same system headers and libraries that will installed on the *target* system, not the equivalent ones found in the host system in ``/usr/include`` and ``/usr/lib``.
  If this is not done then we can have a cross-build that *more or less* works for simple applications if the host and target systems are close enough, but might misteriously fail (at build- or run-rime) with more complex applications.
  
  Some cross compilers targeting linux environments already come with an integrated sysroot that they use in place of the host one. How to check if this is the case is compiler dependent, on GCC this can be done using a command line option::

    aarch64-linux-gnu-gcc --print-sysroot

  If the result is ``/`` or if for any reason the target uses a different sysroot (maybe with some custom drivers pre-installed), then it has to be specified explicitly in the cmake tooolchain file by setting the ``CMAKE_SYSROOT`` variable, see for example in the *linux_raspberry* toolchain::

    set(CMAKE_SYSROOT /rpi/sysroot)

- Additional libraries

  Libraries and drivers should always be compiled from the source tree of the project if possible.
  In case of proprietary packages where the sources are not available, this is not feasible and the only option is to downloand and directly install them inside the build docker.
  As noted before, be sure to always download a specific version.
  On Ubuntu/Debian distributions the tool used to install packages is ``dpkg``. This tools operates by installing (that is unpack + configure) the package inside the sysroot of the host.
  For cross compilation the default behaviour presents two issues:
  
  - dpkg works in the sysroot of the *host* which is not what we want, since what we need is to install the package in the **target** sysroot.

  - the configure step can sometimes invove the execution of tools which are compiled for the target architecture and so will not run if the host uses a different one (e.g. ARM vs x86).

  To avoid the issues above we have to explicitly specify the sysroot to be used and unpack the files, avoiding the *configure* step::
  
    dpkg --root=/rpi/sysroot --unpack mypackage.deb

  If some right issues are found, the following command can also be used instead:

    dpkg -x mypackage.deb /rpi/sysroot

  In the rare cases where the configuration step is necessary but the architecture is different, dpkg has to be executed using in simulation using ```qemu``` (see for example linux_rasperry).
  For packages installed outside the sysroot they can added to the include and link search paths using the appropriate cmake options::
  
    include_directories(SYSTEM dir1 [dir2 ...])
    link_directories(directory1 directory2 ...)
  

Notes
-----
More info on cross-compilaton can be found in several sites online, for example:
http://www.fabriziodini.eu/posts/cross_compile_tutorial/

