# 64-bits ARMv8-a + NVIDIA GPU Linux
set(TOOLCHAIN_FILE ${CMAKE_CURRENT_LIST_DIR}/toolchain.cmake)
include(${TOOLCHAIN_FILE})

set(PKG_CBLAS_LIB "arm-gemm" CACHE STRING "Default CBLAS library")

set(LPDNN_PLUGINS "gpu_cudnn,gpu_gemm,cpu_gemm,cpu_vanilla" CACHE STRING "Configure plugins")

# Enable TensorRT for face detection
set(ENABLE_TENSORRT ON)
