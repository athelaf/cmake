set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR aarch64)

set(CMAKE_SYSROOT /opt/gcc-linaro-6.4.1-2018.05-x86_64_aarch64-linux-gnu/aarch64-linux-gnu/libc)

set(CMAKE_C_COMPILER "aarch64-linux-gnu-gcc")
set(CMAKE_CXX_COMPILER "aarch64-linux-gnu-g++")

list(APPEND CUDA_NVCC_FLAGS "-G -g -O0 --relocatable-device-code=false")
list(APPEND CUDA_NVCC_FLAGS "-gencode arch=compute_50,code=compute_50")
list(APPEND CUDA_NVCC_FLAGS "-gencode arch=compute_50,code=sm_50")
list(APPEND CUDA_NVCC_FLAGS "-arch=sm_50")
list(APPEND CUDA_NVCC_FLAGS "-ccbin aarch64-linux-gnu-gcc")
list(APPEND CUDA_NVCC_FLAGS "-cudart none")
