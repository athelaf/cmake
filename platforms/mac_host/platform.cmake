cmake_minimum_required(VERSION 3.5.0)

set(ENABLE_ARC OFF)

# Default platform options
set(PKG_CBLAS_LIB Accelerate CACHE STRING "Default CBLAS library")

set(LPDNN_GPU_METAL_ENABLE ON)

set(LPDNN_PLUGINS "gpu_metal,cpu_vanilla" CACHE STRING "Configure plugins")


# Default platform options
set(PKG_CBLAS_LIB Accelerate CACHE STRING "Default CBLAS library")
