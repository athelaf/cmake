# 64-bits Linux for ARMv8-a
# Supports the following platforms:
# - RaspberryPi3 / Debian 64 bits
# - ..

set(TOOLCHAIN_FILE ${CMAKE_CURRENT_LIST_DIR}/toolchain.cmake)
include(${TOOLCHAIN_FILE})

set(CMAKE_POSITION_INDEPENDENT_CODE TRUE)

# Select HW architecture
add_definitions(-march=armv8-a)
#add_definitions(-march=armv8.2-a+fp16)

set(CONFIG_CACHE_ARGS
    -DCMAKE_TOOLCHAIN_FILE:STRING=${TOOLCHAIN_FILE}
    -DCMAKE_POSITION_INDEPENDENT_CODE:BOOL=true
    )

# Default platform options
set(PKG_CBLAS_LIB "openblas")
