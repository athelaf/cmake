# 64-bits ARM Linux
# For more details on cross-compiling see:
# https://cmake.org/Wiki/CMake_Cross_Compiling

# Note: NNPACK checks for exact "aarch64"

find_program (CMAKE_UNAME uname /bin /usr/bin /usr/local/bin)

set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR aarch64)

set(CMAKE_C_COMPILER "aarch64-poky-linux-gcc")
set(CMAKE_CXX_COMPILER "aarch64-poky-linux-g++")
set(CMAKE_SYSROOT "/opt/poky/2.4.2/sysroots/aarch64-poky-linux")
