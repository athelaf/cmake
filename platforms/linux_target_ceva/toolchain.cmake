# ARM Linux
# For more details on cross-compiling see:
# https://cmake.org/Wiki/CMake_Cross_Compiling

set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR armv7-a)

add_definitions(-march=armv7-a -mcpu=cortex-a9 -mfpu=neon -mthumb)

set(CMAKE_C_COMPILER "arm-linux-gnueabihf-gcc")
set(CMAKE_CXX_COMPILER "arm-linux-gnueabihf-g++")
