# Linux for ARMv7-a cortex-a9

set(TOOLCHAIN_FILE ${CMAKE_CURRENT_LIST_DIR}/toolchain.cmake)
include(${TOOLCHAIN_FILE})

# Default platform options
set(PKG_CBLAS_LIB openblas CACHE STRING "Default CBLAS library")
set(LPDNN_PLUGINS "cpu_gemm,cpu_vanilla" CACHE STRING "Configure plugins")
