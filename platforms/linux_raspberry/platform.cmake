set(TOOLCHAIN_FILE ${CMAKE_CURRENT_LIST_DIR}/toolchain.cmake)
include(${TOOLCHAIN_FILE})

# Default platform options
set(PKG_CBLAS_LIB arm-gemm CACHE STRING "Default CBLAS library")
set(LPDNN_PLUGINS "cpu_arm_conv,cpu_gemm,cpu_vanilla" CACHE STRING "Configure plugins")
