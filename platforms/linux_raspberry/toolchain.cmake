set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR armv7l)
# Raspberry3 uses a Cortex-A53 processor which is actually armv8-A.
# Armv8-A architecture supports AArch64 in 64-bits mode and is fully backward
# compatibile with Armv7 architecture when used in 32-bits mode.
# We are in 32-bits mode here so we specify armv7-a to avoid ambiguity.
# (also "armv8-a" is not supported by NNPACK).

set(CMAKE_SYSROOT /rpi/sysroot)

set(CMAKE_C_COMPILER arm-linux-gnueabihf-gcc)
set(CMAKE_CXX_COMPILER arm-linux-gnueabihf-g++)

# Specify architecture.
# The -mfp16-format option is needed to avoid errors when compiling NNPACK:
# https://github.com/Maratyszcza/NNPACK/issues/94
# We need to compile in ARM mode (not thumb) since openblas contains ARM assembly language
add_definitions(-march=armv7-a -mfpu=neon-vfpv4 -mfp16-format=ieee -marm -D__ARM_NEON)

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
#set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
#set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
#set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
