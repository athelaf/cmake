# Default platform options
set(PKG_CBLAS_LIB "mkl" CACHE STRING "Default CBLAS library")
set(LPDNN_PLUGINS "cpu_gemm,cpu_vanilla" CACHE STRING "Configure plugins")
