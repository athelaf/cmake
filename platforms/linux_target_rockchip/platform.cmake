# 64-bits Linux for ARMv8-a
# Supports the following platforms:
# - Rockchip ROCK960

set(TOOLCHAIN_FILE ${CMAKE_CURRENT_LIST_DIR}/toolchain.cmake)
include(${TOOLCHAIN_FILE})

# Select HW architecture
add_definitions(-march=armv8-a)

# Default platform options
set(PKG_CBLAS_LIB arm-gemm CACHE STRING "Default CBLAS library")
set(LPDNN_PLUGINS "cpu_gemm,cpu_vanilla" CACHE STRING "Configure plugins")

