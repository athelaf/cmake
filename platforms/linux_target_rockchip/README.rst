HTTP Worker Deployment
======================

Download the Ubuntu image
-------------------------

Here are the files to download for a ROCK960 type B board. More details and images are available here:
https://www.96boards.org/documentation/consumer/rock/downloads/ubuntu.md.html

::

  wget https://dl.vamrs.com/products/rock960/images/ubuntu/rock960_ubuntu_server_16.04_arm64_20180115.tar.gz
  tar -xvf rock960_ubuntu_server_16.04_arm64_20180115.tar.gz

::

  wget https://dl.vamrs.com/products/rock960/images/ubuntu/partitions/u-boot/rk3399_loader_v1.08.106.bin

Flash eMMC
----------

Follow the steps described on following link, to flash the downloaded image to eMMC.

https://www.96boards.org/documentation/consumer/rock/installation/linux-mac-rkdeveloptool.md.html

Board setup
-----------

Here is a set of commands to run once to setup the network and update some packages. Execute the following commands on the board. In order to do it, connect a screen and keyboard to the board.

Configure the connection to WIFI
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Following tool can be used to configure the WIFI connection::

  $ nmtui

Get IP address of the board
~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

  $ ifconfig wlan0

Update packages
~~~~~~~~~~~~~~~

::

  $ sudo apt update
  $ sudo apt install libstdc++6
  $ sudo apt install libgomp1


Worker setup
------------

Once the board setup is done, you can connect remotely to the board using SSH. First copy the NVISO SDK files to the board, then connect and start the worker.

Copy NVISO files to the board
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On the host computer.

::

  cd $SDK_DELIVERY_FOLDER
  # Password: rock
  scp -r lib rock@$BOARD_IP_ADDR:/home/rock
  scp -r bin rock@$BOARD_IP_ADDR:/home/rock
  scp -r data rock@$BOARD_IP_ADDR:/home/rock

Execute the worker on the board
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On the host computer, connect to the board using SSH.

::

  ssh rock@$BOARD_IP_ADDR     # Password: rock

In order to use the fastest CPU you can find the id of one of it's core to use it. Search the two CPU core ids of the dual core CPU, by looking at cpuinfo output and searching for the following informations: CPU part = 0xd08 and CPU revision = 2.

::

  $ cat /proc/cpuinfo

Execute the worker on the dual core CPU, using taskset.

::

  $ cd /home/rock
  $ export LD_LIBRARY_PATH=./lib
  $ taskset -c 4 ./bin/nv3dfi-worker --port 1984    # 4 is the id of the CPU core to use

.. note::

  Once the worker is running, you can send images for processing from the host computer, using board IP address and worker port.
