# 64-bits ARM Linux
# For more details on cross-compiling see:
# https://cmake.org/Wiki/CMake_Cross_Compiling

# Note: NNPACK checks for exact "aarch64"


set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR aarch64)

set(CMAKE_C_COMPILER "aarch64-linux-gnu-gcc")
set(CMAKE_CXX_COMPILER "aarch64-linux-gnu-g++")
