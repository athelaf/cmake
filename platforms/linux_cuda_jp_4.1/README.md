# Linux CUDA target Platform

## Dockerfile
The docker file required the following pakages from the JetPack installation:

- cuda-repo-l4t
- libcudnn

Those files are usually downloaded and installed with the installer provided by Nvidia.
Since this installer uses a graphical interface to download and install them, the docker has to do download and install by itself. 

### Jetpack requirements 

Download the last version of the JetPack:
[Nvidia download center](https://developer.nvidia.com/embedded/downloads)  (JetPack)

Run it, on supported distribution, until arrived in the download center. 

A file has been created in the installation folder that has been chosen previously:
```
jetpack_download/repository.json
```
It contains all file that the URL of every package in the JetPack installation.


The docker file should donwload and extract those files:

- cuda-repo-l4t and all the packages that are in it.  
- libcudnn  
- libcudnn _dev  
- libcudnn _doc  
