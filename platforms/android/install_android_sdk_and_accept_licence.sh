expect -c '
    set timeout -1;
    spawn tools/bin/sdkmanager "platforms;android-26" --sdk_root=android-sdk-26
    expect {
        Accept?* { exp_send "y\r"; exp_continue }
        eof
    }'

