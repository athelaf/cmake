set(CMAKE_SYSTEM_NAME Android)

# API level, supports Android 5.0
set(CMAKE_SYSTEM_VERSION 21)

if(CMAKE_FORCE_64BIT)
  set(CMAKE_ANDROID_ARCH_ABI arm64-v8a)
else()
  set(CMAKE_ANDROID_ARCH_ABI armeabi-v7a)
  # Choice is between "arm" (32bit instructions) and "thumb" (16bit instructions)
  # Some ARM assembly in OpenBLAS used to require "arm"
  set(CMAKE_ANDROID_ARM_MODE arm)
  set(CMAKE_ANDROID_ARM_NEON ON)
endif()

#Use LLVM libc++ stl.
#gnustl fails because of many unimplemented c++11 functions
set(CMAKE_ANDROID_STL_TYPE c++_shared)

#Default value for cmake is gcc but NDK default is clang
#and support for gcc is deprecated 
set(CMAKE_ANDROID_NDK_TOOLCHAIN_VERSION clang)

# To be verified:
# When using NDK Cmake support we have to enable rtti and exceptions likewise:
#   set(ANDROID_CPP_FEATURES "rtti exceptions")
#   set(ANDROID_PIE ON)
