# iOS toolchain variables.
set(ENABLE_VISIBILITY ON)
set(IOS_PLATFORM OS)
set(IOS_ARCH arm64)
set(ENABLE_ARC OFF CACHE BOOL "iOS: enable Automatic Reference Counting, switch ON for Swift or Objective-C!")
set(IOS_DEPLOYMENT_TARGET "11.3" CACHE STRING "iOS minimum deployment target")
set(TOOLCHAIN_FILE ${CMAKE_CURRENT_LIST_DIR}/ios_toolchain.cmake)
include(${TOOLCHAIN_FILE})

include(${CMAKE_CURRENT_LIST_DIR}/set_framework_properties.cmake)

# Default platform options
set(PKG_CBLAS_LIB Accelerate CACHE STRING "Default CBLAS library")
# FIXME gpu_metal does not compile
set(LPDNN_GPU_METAL_ENABLE OFF)
set(LPDNN_PLUGINS "cpu_gemm,cpu_vanilla" CACHE STRING "LPDNN plugin configuration")
