include(ExternalProject)

set(macro_internal_dir ${CMAKE_CURRENT_LIST_DIR} CACHE INTERNAL "")

macro(add_cmake_project PROJECT PATH ARGS)

  # FIXME: pass the EXT_LIBS list so that we can set them as BUILD_BYPRODUCTS
  # in ExternalProject_Add() call below
  #    set(BYPRODUCTS "")
  #    foreach(LIB ${ARGN})
  #        set(BYPRODUCTS ${BYPRODUCTS} ${CMAKE_BINARY_DIR}/deps/lib/lib${LIB}.a)
  #    endforeach()

  # Compile everything by using VC++ static runtime
  if(WIN32 OR MSVC)
    set(DEFAULT_CACHE_ARGS
      -DCMAKE_CXX_FLAGS_RELEASE:STRING=/MT
      -DCMAKE_CXX_FLAGS_DEBUG:STRING=/MTd
      )
  endif()

  if (ENV_TO_SET)
    #set(CFG_CMD "cmake -E env ${ENV_TO_SET} ls")
    set(CFG_CMD "export ${ENV_TO_SET}")
  endif()

  if(CMAKE_CROSSCOMPILING)
    set(DEFAULT_CACHE_ARGS ${DEFAULT_CACHE_ARGS}
      -DCMAKE_TOOLCHAIN_FILE:STRING=${TOOLCHAIN_FILE})
  else()
    set(DEFAULT_CACHE_ARGS ${DEFAULT_CACHE_ARGS}
      -DCMAKE_C_COMPILER:STRING=${CMAKE_C_COMPILER}
      -DCMAKE_CXX_COMPILER:STRING=${CMAKE_CXX_COMPILER})
  endif()

  if(IOS)
    set(DEFAULT_CACHE_ARGS ${DEFAULT_CACHE_ARGS}
      -DIOS_PLATFORM:STRING=${IOS_PLATFORM} -DIOS_ARCH:STRING=${IOS_ARCH}
      -DIOS_DEPLOYMENT_TARGET:STRING=${IOS_DEPLOYMENT_TARGET})
  endif()

  if(APPLE)
    set(DEFAULT_CACHE_ARGS ${DEFAULT_CACHE_ARGS}
      -DENABLE_ARC:BOOL=${ENABLE_ARC})
  endif()

  set(DEFAULT_CACHE_ARGS ${DEFAULT_CACHE_ARGS}
    -DCMAKE_POSITION_INDEPENDENT_CODE:BOOL=true)

  if(CONFIG_CACHE_ARGS)
    set(DEFAULT_CACHE_ARGS ${DEFAULT_CACHE_ARGS} ${CONFIG_CACHE_ARGS})
  endif()
  ExternalProject_Add(${PROJECT}
    PREFIX "${CMAKE_BINARY_DIR}"
    # Disable download, patch and update steps
    DOWNLOAD_COMMAND ""
    PATCH_COMMAND ""
    UPDATE_COMMAND ""
    CMAKE_COMMAND cmake -E env ${ENV_TO_SET} cmake
    SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/${PATH}"
    INSTALL_DIR "${CMAKE_BINARY_DIR}"
    CMAKE_ARGS ${ARGS} -DCMAKE_PREFIX_PATH=${CMAKE_BINARY_DIR}/deps -DCMAKE_INSTALL_MESSAGE=LAZY -DCMAKE_BUILD_TYPE=RELEASE # better than ${CMAKE_BUILD_TYPE} as the library name(s) can change.
    CMAKE_CACHE_DEFAULT_ARGS "${DEFAULT_CACHE_ARGS}"
    )
endmacro()
