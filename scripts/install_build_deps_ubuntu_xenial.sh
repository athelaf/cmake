#!/bin/bash

# Exit on error
set -e

# Install build dependencies
apt-get update && apt-get install -y --no-install-recommends \
  software-properties-common \
  build-essential \
  ninja-build \
  wget \
  xz-utils \
  fakeroot \
  curl \
  unzip \
  git \
  python \
  python-pip \
  python-setuptools \
  python3 \
  python3-pip \
  python3-setuptools \
  python3-dev \
  file \
  vim \
  hdf5-tools \
  libhdf5-dev \
  python3-wheel \
  graphviz \
  doxygen

# Install python packages (needed for models conversion)
pip3 install --upgrade pip==9.0.3  # We don't yet support pip v10
pip3 install numpy==1.14.5
pip3 install Pillow==5.2.0
pip3 install h5py==2.8.0
pip3 install protobuf==3.6.0
pip3 install PyYAML==3.12
pip3 install jsonschema==2.6.0
pip3 install termcolor==1.1.0
pip3 install tabulate==0.8.2

pip3 install docker==2.5.1
pip3 install requests==2.18.4
pip3 install Sphinx==1.7.6
pip3 install https://github.com/lkeller-nviso/sphinx-jsonschema/archive/master.zip
pip3 install sphinxcontrib-httpdomain==1.7.0
pip3 install breathe==4.9.1
pip3 install onnx==1.3.0
pip3 install cbor2

# Install utility packages that ease the dev
pip3 install colored-traceback

# Install cmake version used by the sdk
curl https://cmake.org/files/v3.12/cmake-3.12.3-Linux-x86_64.tar.gz | tar zxf - && \
    ln -sf /build/cmake-3.12.3-Linux-x86_64/bin/cmake /usr/bin/cmake
