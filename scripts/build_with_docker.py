#!/usr/bin/env python3


""" Script to build using docker """

# pylint: disable=too-many-branches
# pylint: disable=too-many-statements

import os
import sys
import argparse
import subprocess
import shutil
import re
import multiprocessing


def execute_cmd(cmd, subcmd="", working_dir=None, verbose=False):
    """ Execute a command, with verbosity and return status """

    ret = True
    try:
        if verbose:
            cmd_tot = cmd
            if subcmd != "":
                cmd_tot += " \'" + subcmd + "\'"
            if working_dir is not None:
                print("\n>> " + cmd_tot + "   (from: " + working_dir + ")\n")
            else:
                print("\n>> " + cmd_tot + "\n")
        use_shell = False
        if os.name == "nt":
            use_shell = True
        if subcmd != "":
            cmd_list = cmd.split(" ")
            cmd_list.append(subcmd)
            if verbose:
                ret = (subprocess.call(cmd_list, shell=use_shell) == 0)
                return ret, []
            # proc = subprocess.Popen(cmd_list, cwd=working_dir,
            #                         stdout=subprocess.PIPE, shell=use_shell)
        else:
            proc = subprocess.Popen(cmd.split(" "), cwd=working_dir,
                                    stdout=subprocess.PIPE, shell=use_shell)
        stdout, stderr = proc.communicate()
        lines = stdout.decode('UTF-8').split('\n')
        if stderr:
            lines += "stderr:"
            lines += stderr.decode('UTF-8').split('\n')
        if proc.returncode != 0:
            if verbose:
                print("Command return code " + str(proc.returncode))
            ret = False
        if verbose:
            for line in lines:
                print(line)
        return ret, lines
    except OSError as exception:
        print(exception)
        ret = False
    except subprocess.TimeoutExpired as timeout:
        print(timeout)
        ret = False
    return ret, []


def get_parent_platform(dockerfile):
    """ Return a string containing the name of the parent platform if any """
    with open(dockerfile) as dockerfile_fp:
        dockerfile_content = dockerfile_fp.read()

    match = re.search(r'^FROM[ \t]+(.+)$', dockerfile_content, re.MULTILINE)
    if match:
        parent = match.group(1)
        is_build_env_docker = re.match(r'^build-env-([^ \t#]+)$', parent)
        if is_build_env_docker:
            parent_platform = is_build_env_docker.group(1)
            return parent_platform
    return None


def build_docker_image(sdk_dir, platform):
    """ Build docker image for the specified platform (recursively) """
    context_dir = os.path.join(sdk_dir, "cmake", "pkg-cmake")
    docker_file = os.path.join(context_dir, "platforms", platform, "Dockerfile")
    docker_name = "build-env-" + platform

    # Build parent image if it comes from another platform
    parent_platform = get_parent_platform(docker_file)
    if parent_platform:
        print("Building docker for parent platform: ", parent_platform)
        build_docker_image(sdk_dir, parent_platform)

    # Build docker image
    cmd = "docker image build --quiet -t " + docker_name + " -f " + docker_file + " " + context_dir
    rets = execute_cmd(cmd, working_dir=sdk_dir, verbose=True)
    if not rets[0]:
        sys.exit(-1)
    return docker_name


def main():
    """ Entry point """

    target_platform_default = "linux_host"
    sdk_dir = os.path.realpath(__file__)
    for _ in range(4): sdk_dir = os.path.dirname(sdk_dir)  # cd ..

    parser = argparse.ArgumentParser()
    parser.add_argument("--platform", type=str,
                        help="Build target platform",
                        default=target_platform_default)
    parser.add_argument("--product", type=str,
                        help="Build product",
                        default="default")
    parser.add_argument("--build_dir", type=str,
                        help="Build directory",
                        default=None)
    parser.add_argument("--sdk_dir_host", type=str,
                        help="Sdk directory on host (for build-in-docker-container)",
                        default=None)
    parser.add_argument("--build_dir_host", type=str,
                        help="Build directory on host (for build-in-docker-container)",
                        default=None)
    parser.add_argument("--user", type=str,
                        help="User in container (for build-in-docker-container) \
                        user_id:group_id",
                        default=None)
    parser.add_argument("--cmake_options", type=str,
                        help="Options to forward to cmake command line, for example:" +
                        "\n\"-DLPDNN_ENABLE_BENCHMARK=ON\"" +
                        "\n\"-DLPDNN_ENABLE_BENCHMARK:BOOL=ON\"" +
                        "\n\"-DLPDNN_PLUGINS=cpu_gemm,cpu_vanilla\"" +
                        "\n\"-DPKG_CBLAS_LIB=openblas\"",
                        default="")
    parser.add_argument("--make_targets", type=str,
                        help="Make target(s) separated by a space",
                        default="install")
    parser.add_argument("--clean_out", action='store_true',
                        help="Remove the output folder before the build")

    args = parser.parse_args()
    if args.build_dir:
        # Use the specified output directory
        build_dir = args.build_dir
    else:
        build_dir = sdk_dir + "-build"
    if not args.platform:
        args.platform = target_platform_default
    # Append config name as subdir of output directory
    build_dir = os.path.join(build_dir, args.platform)
    if args.build_dir_host:
        args.build_dir_host = os.path.join(args.build_dir_host, args.platform)

    if not args.sdk_dir_host:
        args.sdk_dir_host = sdk_dir
    if not args.build_dir_host:
        args.build_dir_host = build_dir
        # Create and clean build directory if necessary
        # This is not done if a host build directory is specified because in that
        # case we are running inside a docker so we don't have access to the host dir
        if (args.clean_out) and (os.path.isdir(build_dir)):
            print("Info: clean out folder")
            shutil.rmtree(build_dir)
        os.makedirs(build_dir, exist_ok=True)

    print("platform: ", args.platform)
    print("build path: ", build_dir)

    # Mount the sdk and build directory inside the build docker at the same path
    # we have in the host machine. This is not necessary but make life easier since the
    # debugging symbols then point to the correct pathnames.
    sdk_docker_path = args.sdk_dir_host
    build_docker_path = args.build_dir_host

    # Get user and group id
    try:
        if args.user:
            id_s = args.user.split(':')
            user_id = id_s[0]
            group_id = id_s[1]
        else:
            if os.name == "nt":
                rets = execute_cmd("wmic useraccount where name='%username%' get sid")
                temp = rets[1][1]
                user_id = (temp.split('-')[-1]).strip()
                group_id = user_id
            else:
                rets = execute_cmd("id -u")
                user_id = rets[1][0]
                rets = execute_cmd("id -g")
                group_id = rets[1][0]
        if (not user_id.isdigit()) or (not group_id.isdigit()):
            raise Exception
    except BaseException:
        print("Error: failed to get user and group id !!")
        sys.exit(1)

    # Build the docker image
    print("Building docker for: ", args.platform)
    docker_name = build_docker_image(sdk_dir, args.platform)

    # Build all the applications using the docker
    build_cores = multiprocessing.cpu_count()
    print("Building...")
    cmd = "docker run --rm -i -u " + user_id + ":" + group_id + \
        " -v " + args.sdk_dir_host + ":" + sdk_docker_path + \
        " -v " + args.build_dir_host + ":" + build_docker_path + \
        " " + docker_name + \
        " bash -c"
    subcmd = "cd " + build_docker_path + " ; " + \
        "cmake -G Ninja " + sdk_docker_path + \
        " -DPRODUCT=" + args.product + \
        " -DPLATFORM=" + args.platform + \
        " -DCMAKE_ANDROID_NDK=$ANDROID_NDK_HOME " + \
        " " + args.cmake_options + \
        " ; " + \
        "ninja 3>&1 1>&2 2>&3 -j" + str(build_cores) + " " + args.make_targets

    rets = execute_cmd(cmd, subcmd, verbose=True)

    if not rets[0]:
        sys.exit(-1)


if __name__ == "__main__":
    main()
