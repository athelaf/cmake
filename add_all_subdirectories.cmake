#
# this macro includes all subdirectories of a specified folder
#
macro(add_all_subdirectories DIRNAME)

    file(GLOB ALL_FILES ${DIRNAME}/*)

    foreach(FILE_NAME ${ALL_FILES})
        if(IS_DIRECTORY ${FILE_NAME})
            if (EXISTS "${FILE_NAME}/CMakeLists.txt")
                add_subdirectory(${FILE_NAME})
            endif()
        endif()
    endforeach()

endmacro()
